// ////////////////////// L I S T E R     C I R C U I T S
let model = require('../models/circuit.js');
let async = require('async');


module.exports.ListerCircuit = function(request, response) {
    response.title = 'Liste des circuits';
    model.getListeCircuit(function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.listeCircuit = result;
        response.render('listerCircuit', response);
    });
}

module.exports.AffichageCircuit = function(request, response) {
    response.title = 'Détail des circuits';

    let data = request.params.num;

    async.parallel([
            // result 0
            function(callback) {
                model.getListeCircuit(function(err, result) {
                    callback(null, result);
                });
            },
            // result 1
            function(callback) {

                model.getDetailCircuit(data, function(err, result) {
                    callback(null, result);

                });
            }
        ],
        function(err, result) {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }


            response.listeCircuit = result[0];

            response.detailCircuit = result[1];
            response.render('listerCircuit', response);


        });

}