// ////////////////////////////////////////////// A C C U E I L
let model = require('../models/resultats.js');
let async = require('async');

module.exports.Index = function(request, response) {
    response.title = "Bienvenue sur le site de WROOM (IUT du Limousin).";
    async.parallel([
            // result 0
            function(callback) {
                model.getLastRank(function(err, result) {
                    callback(null, result);
                });
            }
        ],
        function(err, result) {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }



            response.last = result[0];


            response.render('home', response);


        });
};
module.exports.NotFound = function(request, response) {
    response.title = "Bienvenue sur le site de SIXVOIX (IUT du Limousin).";
    response.render('notFound', response);
};