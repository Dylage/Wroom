let HomeController = require('./../controllers/HomeController');
let ResultatsController = require('./../controllers/ResultatsController');
let EcurieController = require('./../controllers/EcurieController');
let PiloteController = require('./../controllers/PiloteController');
let CircuitController = require('./../controllers/CircuitController');
let SponsorsController = require('./../controllers/SponsorsController');

// Routes
module.exports = function(app) {
    

    // Routes principales
    app.get('/', HomeController.Connexion);
    app.get('/connexion', HomeController.Connexion);
    app.post('/connexion', HomeController.Connexion);
    app.get('/connexion/:deconnexion', HomeController.Connexion);


    // Pilotes
    app.get('/gestionPilotes', PiloteController.AffichagePilotes);
    app.get('/gestionPilotes/Ajout', PiloteController.AjoutForm);
    app.post('/gestionPilotes/AjoutPilote', PiloteController.AjouterPilote);
    app.get('/gestionPilotes/ModifierPilote/:num', PiloteController.AjoutForm);
    app.get('/gestionPilotes/SupprimerPilote/:num', PiloteController.SupprimerPilote);
    app.post('/gestionPilotes/ModifierPilote', PiloteController.ModifierPilote);

    // Photos des pilotes
    app.get('/gestionPilotes/ModifierPhotos/:num', PiloteController.ModifierPhotos);
    app.post('/gestionPilotes/ModifierPhotos/:pilnum/Modifier', PiloteController.ModifierPhoto);
    app.post('/gestionPilotes/ModifierPhotos/:pilnum/Modifier/Supprimer', PiloteController.SupprimerPhoto);
    app.post('/gestionPilotes/ModifierPhotos/:pilnum/Modifier/Ajouter', PiloteController.AjouterPhoto);


    // Circuits
    app.get('/gestionCircuits', CircuitController.ListerCircuit);
    app.get('/gestionCircuits/Ajout', CircuitController.AjoutForm);
    app.post('/gestionCircuits/AjoutCircuit', CircuitController.AjouterCircuit);
    app.get('/gestionCircuits/ModifierCircuit/:num', CircuitController.AjoutForm);
    app.post('/gestionCircuits/ModifierCircuit', CircuitController.ModifierCircuit);
    app.get('/gestionCircuits/SupprimerCircuit/:num', CircuitController.SupprimerCircuit);


    // Ecuries
    app.get('/gestionEcuries', EcurieController.ListerEcurie);
    app.get('/gestionEcuries/Ajout', EcurieController.AjoutForm);
    app.post('/gestionEcuries/AjoutEcurie', EcurieController.AjouterEcurie);
    app.get('/gestionEcuries/ModifierEcurie/:num', EcurieController.AjoutForm);
    app.post('/gestionEcuries/ModifierEcurie', EcurieController.ModifierEcurie);
    app.get('/gestionEcuries/SupprimerEcurie/:num', EcurieController.SupprimerEcurie);


    //Résultats
    app.get('/gestionResultats', ResultatsController.MenuResultats);
    app.post('/gestionResultats/SaisieResultats/', ResultatsController.SaisieResultats);


    // Sponsors
    app.get('/gestionSponsors', SponsorsController.ListerSponsors);
    app.get('/gestionSponsors/Ajout', SponsorsController.AjoutForm);
    app.post('/gestionSponsors/AjoutSponsor', SponsorsController.AjouterSponsor);
    app.get('/gestionSponsors/ModifierSponsor/:num', SponsorsController.AjoutForm);
    app.post('/gestionSponsors/ModifierSponsor', SponsorsController.ModifierSponsor);
    app.get('/gestionSponsors/SupprimerSponsor/:num', SponsorsController.SupprimerSponsor);


    // Tout le reste
    app.get('*', HomeController.NotFound);
    app.post('*', HomeController.NotFound);

};