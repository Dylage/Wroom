let model = require('../models/ecurie.js');
let modelPilote = require('../models/pilote.js');
let modelSponsor = require('../models/sponsor.js');
let modelVoiture = require('../models/voiture.js');

let async = require('async');
const fs = require('fs');

// //////////////////////// L I S T E R  E C U R I E S

module.exports.ListerEcurie = function(request, response) {

    response.title = 'Gestion des écuries';


    async.parallel([
        function(callback) {
            model.getListeEcurie(function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.ecuries = result[0];
        
        response.render('gestionEcuries', response);

    });
};

module.exports.AjoutForm = function(request, response) {
    response.title = 'Ajout d\'une écurie';

    let id = request.params.num; // Id du pilote à modifier

    async.parallel([
        // result[0]
        function(callback) {
            modelPilote.getAllPays(function(err, result) {
                callback(null, result);
            });
        },
        // result[1]
        function(callback) {
            model.getDetailEcurie(id, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }

        response.pays = result[0];
        if (result[1]) {
            response.ecurie = result[1][0];
        }


        
        response.render('gestionEcuries', response);

    });
 

};

module.exports.AjouterEcurie = function(request, response) {
    response.title = 'Ajout d\'une écurie';

    // On récupère le nom de l'image pour le mettre dans la DB
    let imgName = request.files.img.name;

    let ecurie = {
        "ecunom":request.body.nom,
        "ecunomdir":request.body.dir,
        "paynum":request.body.pays,
        "ecuadrsiege":request.body.adr,
        "ecuadresseimage":imgName,
        "ecupoints":request.body.pts,
    };
    

    async.parallel([
        // Ici, on déplace l'image dans son dossier
        function(callback) {
            let img = request.files.img;

            img.mv("../public/public/image/ecurie/" + imgName);
            
            callback(null);
        },
        // Ici, on enregistre l'écurie dans la DB
        function(callback) {
            model.ajouterEcurie(ecurie, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.render('gestionEcuries', response);

    });

}

module.exports.ModifierEcurie = function(request, response) {
    response.title = 'Modification d\'une écurie';

    // On récupère le nom de l'image pour le mettre dans la DB

    let imgName;
    
    if (null != request.files) {
        imgName = request.files.img.name;
    }else{
        imgName = request.body.imgEcurieOriginal;
    }

    let ecurie = {
        "ecunum":request.body.id,
        "ecunom":request.body.nom,
        "ecunomdir":request.body.dir,
        "paynum":request.body.pays,
        "ecuadrsiege":request.body.adr,
        "ecuadresseimage":imgName,
        "ecupoints":request.body.pts,
    };
    

    async.parallel([
        function(callback) {
            // Ajout de la nouvelle image
            if (null != request.files) {
                let img = request.files.img;
                img.mv("../public/public/image/ecurie/" + imgName);
            }

            callback(null);
        },
        function(callback) {
            // Suppression de l'ancienne image
            if (null != request.files) {
                fs.unlink("../public/public/image/ecurie/" + request.body.imgEcurieOriginal, (err) => {
                    if (err) throw err;
                    console.log(request.body.imgEcurieOriginal + ' supprimé');
                    callback(null, null);
                });
            }
        },
        function(callback) {
            model.modifierEcurie(ecurie, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.render('gestionEcuries', response);

    });
 
};

module.exports.SupprimerEcurie = function(request, response) {
    response.title = 'Suppression d\'une écurie';

    let id = request.params.num;

    let adresseImg; // Adresse de l'image à supprimer

    async.parallel([
        // Suppression de la DB
        // On doit passer par des requêtes synchrones car la troisième ne peut s'éxecuter sans les deux premières etc
        function(callback) {
            modelPilote.effacerEcurieDesPilotes(id, function(err, result) {
                if (err) {
                    // gestion de l'erreur
                    console.log(err);
                    return;
                }
        
                modelSponsor.supprimerFinancementDUneEcurie(id, function(err, result) {
                    if (err) {
                        // gestion de l'erreur
                        console.log(err);
                        return;
                    }
            
                
                    modelVoiture.supprimerVoituresDUneEcurie(id, function(err, result) {
                        if (err) {
                            // gestion de l'erreur
                            console.log(err);
                            return;
                        }
                
                    
                        model.getDetailEcurie(id, function(err, result) {
                            if (err) {
                                // gestion de l'erreur
                                console.log(err);
                                return;
                            }

                            adresseImg  = result[0].ecuadresseimage;
                            
                            model.supprimerEcurie(id, function(err, result) {
                                if (err) {
                                    // gestion de l'erreur
                                    console.log(err);
                                    return;
                                }

                                fs.unlink("../public/public/image/ecurie/" + adresseImg, (err) => {
                                    if (err) throw err;
                                    console.log(adresseImg + ' supprimé');
                                    callback(null, null);
                                });
                        
                            });
                
                        });
                
                    });
            
                });
            
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }        
        
        response.suppression = "supp";
                    
        response.render('gestionEcuries', response);

    });

    

};