// ///////////////////////// R E P E R T O I R E    D E S    P I L O T E S
let model = require('../models/sponsor.js');
let modelEcurie = require('../models/ecurie.js');
let modelPhotos = require('../models/photos.js');
let modelCourse = require('../models/course.js');
let modelSponsor = require('../models/sponsor.js');

let async = require('async');

module.exports.ListerSponsors = function(request, response) {
    response.title = 'Gestion des pilotes';


    async.parallel([
        function(callback) {
            model.getAllSponsors(function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.sponsors = result[0];
        
        response.render('gestionSponsors', response);

    });

};

module.exports.AjoutForm = function(request, response) {
    response.title = 'Ajout d\'un sponsor';

    let id = request.params.num; // Id du sponsor à modifier

    async.parallel([
        // result[0]
        function(callback) {
            modelEcurie.getListeEcurie(function(err, result) {
                callback(null, result);
            });
        },
        // result[1]
        function(callback) {
            model.getSponsorParId(id, function(err, result) {
                callback(null, result);
            });
        },
        // result[2]
        function(callback) {
            model.getEcurieFinancee(id, function(err, result) {
                callback(null, result);
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }

        response.ecuries = result[0];

        console.log(result[2]);
        

        if (result[2] && result[2][0]) {
            response.ecurieSponsorisee = result[2];
        }
        

        if (id) {
            response.sponsor = result[1][0];
        }


        
        response.render('gestionSponsors', response);

    });
 

};

module.exports.AjouterSponsor = function(request, response) {
    response.title = 'Ajout d\'un sponsor';

    let sponsor = {
        "sponom":request.body.nom,
        "sposectactivite":request.body.activite,
    };

    console.log(request.body);
    

    async.parallel([
        function(callback) {
            model.ajouterSponsor(sponsor, function(err, result) {
                let sponumId = result.insertId;
                console.log(sponumId);
                
                request.body.ecurie.forEach(id => {                    
                    let financement = {
                        "sponum":sponumId,
                        "ecunum":id,
                    };

                    console.log(financement);
                    
                    model.ajouterFinancement(financement, function(err, result) {
                    });
                });
                callback(null, null);
                
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.render('gestionSponsors', response);

    });
 
};

module.exports.ModifierSponsor = function(request, response) {
    response.title = 'Modification d\'un sponsor';

    let sponsor = {
        "sponum":request.body.id,
        "sponom":request.body.nom,
        "sposectactivite":request.body.activite,
    };

    
    

    async.parallel([
        function(callback) {
            model.modifierSponsor(sponsor, function(err, result) {
                let financement = {
                    "sponum":sponsor.sponum,
                    "ecunum":request.body.ecurie,
                };
                
                model.modifierFinancement(financement, function(err, result) {
                    callback(null, result);
                });            
            });
        }
    ], function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        
        response.render('gestionSponsors', response);

    });
 
};

module.exports.SupprimerSponsor = function(request, response) {
    response.title = 'Suppression d\'un sponsor';

    let id = request.params.num;

    // On doit passer par des requêtes synchrones car la quatrième ne peut s'éxecuter sans les trois premières

    

    model.supprimerPiloteSponsoriseesParSponsor(id, function(err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }

        model.supprimerFinancementDUnSponsor(id, function(err, result) {
            if (err) {
                // gestion de l'erreur
                console.log(err);
                return;
            }

            model.supprimerSponsor(id, function(err, result) {
                if (err) {
                    // gestion de l'erreur
                    console.log(err);
                    return;
                }

                response.suppression = "supp";
        
                response.render('gestionSponsors', response);
            });
    
        });
            
    

    });

    

    

    

     
};